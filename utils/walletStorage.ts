import Cookies from 'js-cookie';

export const userdata = 'userdata';
export const phone = 'phone';
export const otp = 'otp';
export const date = 'date';

export default class WalletStorage {
  store(user: string, phoneNumber: string, dateExpiry: string) {
    Cookies.set(userdata, user);
    Cookies.set(phone, phoneNumber);
    Cookies.set(date, dateExpiry);
  }

  saveOTP(code: string) {
    Cookies.set(otp, code);
  }

  saveExpiry(code: string) {
    Cookies.set(date, code);
  }

  getPhoneNumber() {
    return Cookies.get(phone);
  }

  getUser() {
    const data = Cookies.get(userdata);
    return JSON.parse(data);
  }

  getOTP() {
    return Cookies.get(otp);
  }

  getExpire() {
    return Cookies.get(date);
  }

  clear() {
    this.removePhoneNumber();
    this.removeUser();
    this.removeOTP();
    this.removeExpiry();
  }

  removePhoneNumber() {
    Cookies.remove(phone);
  }

  removeUser() {
    Cookies.remove(userdata);
  }

  removeOTP() {
    Cookies.remove(otp);
  }

  removeExpiry() {
    Cookies.remove(date);
  }
}
