import {Store} from 'vuex';

export interface Snackbar {
  show(message: string, color: string, options?: Snackbar): void;
  success(message: string): void;
  error(message: string): void;
  info(message: string): void;
}

export default class SnackbarHelper implements Snackbar {
  $store: Store<any>;

  constructor(store) {
    this.$store = store;
  }

  show(message: string, color: string, options?: Snackbar) {
    this.$store.dispatch('snackbar/setSnackbar', {
      content: message,
      color,
      ...options,
    });
  }

  success(message: string) {
    this.show(message, 'success');
  }

  error(message: string) {
    this.show(message, 'error');
  }

  info(message: string) {
    this.show(message, 'info');
  }
}
