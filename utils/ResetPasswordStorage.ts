import Cookies from 'js-cookie';

export const PASSWORD_SALT = 'password_salt';
export const PASSWORD_PHONE = 'password_phone';
export const PASSWORD_OTP = 'password_otp';

export default class ResetPasswordStorage {
  store(salt: string, phoneNumber: string) {
    Cookies.set(PASSWORD_SALT, salt);
    Cookies.set(PASSWORD_PHONE, phoneNumber);
  }

  saveOTP(code: string) {
    Cookies.set(PASSWORD_OTP, code);
  }

  getPhoneNumber() {
    return Cookies.get(PASSWORD_PHONE);
  }

  getSalt() {
    return Cookies.get(PASSWORD_SALT);
  }

  getOTP() {
    return Cookies.get(PASSWORD_OTP);
  }

  clear() {
    this.removePhoneNumber();
    this.removeSalt();
    this.removeOTP();
  }

  removePhoneNumber() {
    Cookies.remove(PASSWORD_PHONE);
  }

  removeSalt() {
    Cookies.remove(PASSWORD_SALT);
  }

  removeOTP() {
    Cookies.remove(PASSWORD_OTP);
  }
}
