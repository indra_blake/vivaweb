---
title: Kebijakan Privasi
---

# Kebijakan Privasi

#### Selamat datang di aplikasi Viva Apotek dan/atau shop.vivahealth.co.id

Disarankan sebelum mengakses Situs ini lebih jauh, Anda terlebih dahulu membaca dan memahami syarat dan ketentuan yang berlaku, serta Kebijakan Privasi terkait dengan data dan informasi pribadi milik Anda. Kebijakan Privasi ini memberikan informasi mengenai perlindungan data pribadi Anda, serta menjelaskan bagaimana cara Kami mengumpulkan, menggunakan, membagikan, serta tindakan yang Kami ambil untuk menjaga keamanan dan kerahasiaan data Anda.
 
**1. DEFINISI**

  Setiap kata atau istilah berikut yang digunakan di dalam Kebijakan Privasi ini memiliki arti seperti berikut di bawah, kecuali jika kata atau istilah yang bersangkutan di dalam pemakaiannya dengan tegas menentukan lain.
  
   1.1. "**Kami**", berarti PT Sumber Hidup Sehat atau Viva Health Indonesia selaku pemilik dan pengelola Situs www.vivahealth.co.id dan/atau mobile application yang bernama Viva Apotek.

   1.2. "**Anda**", berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami.

   1.3. "**Layanan**", berarti tiap dan keseluruhan jasa serta informasi yang ada pada Situs www.vivahealth.co.id, dan tidak terbatas pada informasi yang disediakan, layanan aplikasi dan fitur, dukungan data, serta aplikasi mobile yang Kami sediakan.

   1.4. "**Pengguna**", berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, termasuk diantaranya Pengguna Belum Terdaftar dan Pengguna Terdaftar.

   1.5. "**Pengguna Belum Terdaftar**", berarti tiap orang yang mengakses Situs Kami dan belum melakukan registrasi.

   1.6. "**Pengguna Terdaftar**", berarti tiap orang yang mengakses dan menggunakan layanan dan jasa yang disediakan oleh Kami, serta telah melakukan registrasi dan memiliki akun pada Situs Kami.

   1.7. "**Pihak Ketiga**", berarti pihak lainnya, termasuk namun tidak terbatas mitra penyedia layanan pembayaran, logistik atau kurir, infrastruktur situs dan mitra-mitra lainnya. 

   1.8. "**Informasi Pribadi**", merupakan tiap dan seluruh data pribadi yang diberikan oleh Pengguna di Situs Kami, termasuk namun tidak terbatas pada nama lengkap, tempat dan tanggal lahir, jenis kelamin, alamat, nomor identitas KTP, lokasi pengguna, kontak pengguna, serta dokumen dan data pendukung lainnya sebagaimana diminta pada ringkasan pendaftaran akun serta pada ringkasan aplikasi pengajuan.

   1.9. "**Konten**", berarti teks, data, informasi, angka, gambar, grafik, foto, audio, video, nama pengguna, informasi, aplikasi, tautan, komentar, peringkat, desain, atau materi lainnya yang ditampilkan pada Situs.

   1.10. “**Situs**”, berarti www.vivahealth.co.id
 
**2. PENGUMPULAN INFORMASI PRIBADI**

   Kami mengumpulkan Informasi Pribadi mengenai Anda sebagai Pengguna saat mengakses atau menggunakan Situs mengacu sesuai dengan ketentuan peraturan perundang-undangan yang berlaku. 

   Kami juga mengumpulkan informasi tidak langsung yang tidak dapat diidentifikasi secara pribadi yang mungkin terkait dengan Anda selaku Pengguna, termasuk nama keanggotaan, alamat IP dan informasi login, akses ke kamera yang digunakan untuk mengidentifikasi Pengguna, akses ke GPS untuk dapat memberikan info posisi Pengguna, serta hal-hal lain termasuk preferensi pencapaian dan demografi. Informasi pribadi Anda hanya digunakan untuk kepentingan yang terkait yang telah disetujui oleh Pengguna.
 
**3. PENGGUNAAN DAN PEMBAGIAN INFORMASI PRIBADI**
 
   Dalam penggunaan Anda baik sebagai Pengguna Belum Terdaftar ataupun sebagai Pengguna Terdaftar, Kami akan mengumpulkan beberapa Informasi Pribadi sebagai identifikasi Anda. Informasi pribadi tersebut juga mungkin akan Kami sampaikan kepada Pihak Ketiga dalam hal pemrosesan kegiatan yang berhubungan dengan pengajuan Anda terhadap suatu produk atau layanan tertentu. Kami tidak menyimpan password dari akun Anda, serta tidak akan menanyakan password dari akun Anda. Dengan Anda telah memberikan Informasi Pribadi, berarti Anda memahami dan menyetujui bahwa Anda melakukan pengajuan atas diri pribadi dan tanpa ada paksaan ataupun tekanan dari pihak manapun, dan hanya digunakan untuk kepentingan tersebut di atas. 

   Pengguna bertanggung jawab atas seluruh kebenaran informasi yang diberikan dalam Situs Kami, Pengguna bertanggungjawab penuh terhadap kebenaran dari informasi dan akibat yang ditimbulkan dari kekeliruan informasi tersebut. 
  
   Selain hal tersebut diatas, Kami juga menggunakan Informasi Pribadi dalam kapasitas Kami sebagai pemberi layanan kepada Anda untuk hal sebagai berikut :
  
  1. Memproses segala bentuk permintaan, aktivitas maupun transaksi yang dilakukan oleh Pengguna melalui Situs, termasuk untuk keperluan pengiriman produk kepada Pengguna. 
    
  2. Memberikan akses kepada Anda untuk dapat mendaftar keanggotaan. 
    
  3. Meningkatkan mutu layanan dan informasi yang Kami sampaikan di Situs, dengan menggunakan Informasi Pribadi sebagai data tren, analisa, sampel penelitian, riset pasar, dan data demografi.
    
  4. Memungkinkan Pihak Ketiga untuk menghubungi dan membantu Anda pada saat Anda tertarik terhadap suatu produk dan menyatakan minat dengan mengirimkan Informasi Pribadi kepada Kami.
 
  5. Membantu Pengguna pada saat berkomunikasi dengan Layanan Pangguna Situs, diantaranya untuk :
     - Memeriksa dan mengatasi permasalahan Pengguna
     - Mengarahkan pertanyaan Pengguna kepada petugas layanan pelanggan untuk mengatasi permasalahan
     - Memungkinkan Kami untuk dapat menyampaikan informasi mengenai rilis produk terbaru, promosi, survei, berita, perkembangan terbaru atau layanan mendatang yang akan Kami sampaikan di Situs.
 
  6. Memproses transaksi antara Anda dan Pihak Ketiga dengan dasar Pihak Ketiga membutuhkan Informasi Pribadi sebagai verifikasi dan data pendukung keputusan.

**4. KEAMANAN, PENYIMPANAN dan PENGHAPUSAN DATA INFORMASI PRIBADI PENGGUNA**

   Walaupun Kami telah menggunakan upaya terbaiknya untuk mengamankan dan melindungi Informasi Pribadi Pengguna, perlu diketahui bahwa pengiriman data melalui internet tidaj pernah sepenihnya aman. Dengan demikian, Kami tidak dapat menjamin 100% keamanan data yang disediakan atau dikirimkan kepada Kami oleh Pengguna dan pemberian informasi oleh Pengguna merupakan risiko yang ditanggung oleh Pengguna sendiri. 
  
   Kami akan menyimpan Informasi Pribadi Anda secara rahasia dan tidak membuka atau memberikan informasi tersebut kepada atau memungkinkan akses oleh pihak lain, selain pihak berikut:
   - Anak perusahaan dan perusahaan terkait.
 
   - Pihak Ketiga, dimana produk atau layanannya ditampilkan pada Situs Kami dan menyediakan kutipan web untuk produk dan/atau layanan yang diminta oleh Anda. Pihak Ketiga ini bertindak sebagai pengendali informasi data Anda sehingga Anda harus membaca Kebijakan Privasi mereka atau menghubungi mereka secara langsung untuk informasi tambahan.
 
   - Lembaga Pemerintahan terkait, dalam rangka untuk mencegah penipuan dan pencucian uang, dalam kasus bahwa data palsu atau tidak tepat dan/atau diduga kasus penipuan, yang diatur sesuai dengan ketentuan peraturan perundang-undangan.
 
   - Dalam keadaan tertentu dan luar biasa, Kami mungkin diperlukan untuk mengungkapkan Informasi Pribadi. Kami berkomitmen untuk mematuhi sesuai dengan hukum atau perintah pengadilan, atau dalam menanggapi permintaan yang sah oleh Pemerintah atau Penegak Hukum, atau dalam hal Kami percaya dengan itikad baik bahwa pengungkapan diperlukan, termasuk namun tidak terbatas pada perlindungan hak atau properti Kami, atau untuk mengidentifikasi, mengontak dan melaksanakan tindakan hukum terhadap setiap pihak yang mungkin bisa menyebabkan kerusakan atau gangguan hak-hak Kami atau properti (baik sengaja atau tidak), atau ketika orang lain dapat dirugikan oleh kegiatan tersebut.

   Data Pribadi Anda akan Kami simpan paling tidak sampai dengan 5 tahun sejak Anda berakhir menjadi pengguna Situs Kami atau sesuai dengan persyaratan dalam peraturan perundang-undangan, yang Kami simpan pada pusat data Kami serta pusat pemulihan bencana Kami yang berlokasi di Indonesia. Apabila Anda ingin agar Data Pribadi Anda dihapus sebelum itu, maka Anda dapat mengirimkan permohonan kepada team itadmin@vivahealth.co.id

   Kami juga berusaha untuk menyediakan layanan terbaik untuk Anda. Untuk melakukan hal ini, Kami merekam data cookies. Ketika Pengguna mengunjungi Situs, server Kami akan mengingat informasi yang dikirimkan oleh browser, termasuk diantaranya: alamat IP, jenis browser, website atau laman dikunjungi sebelumnya pengguna datang ke Situs Kami, preferensi Pengguna, serta waktu yang dihabiskan dalam mengakses dan menggunakan Situs Kami.
   
   Cookies ini dapat digunakan untuk kepentingan sebagai berikut :
   - Memberikan Anda pengalaman yang lebih personal di Situs Kami. Hal ini memungkinkan Kami untuk mengingat Anda selaku Pengguna, ketika Anda mengunjungi Situs ini. Anda juga dapat mengakses kutipan Anda yang telah disimpan sebelumnya.
   - Mempelajari kebiasaan atau tren penggunaan akses di Situs ini. Semakin Kami mengerti produk apa yang dibutuhkan oleh Pengguna, diharapkan akan semakin baik kemampuan Kami untuk menawarkan solusi dan layanan yang ditampilkan di Situs.
   - Memperoleh informasi mengenai jumlah pengunjung ke Situs ini dan laman mana yang paling sering diakses atau dikunjungi Pengguna.

   
   www.vivahealth.co.id dapat mengizinkan perusahaan lain menggunakan cookie pada layanan kami. Informasi ini dapat digunakan untuk, antara lain, menentukan popularitas konten tertentu, pengembangan situs dan konten Viva Apotek, mencegah penipuan atau aktivitas ilegal atau tak berwenang lainnya serta untuk mengukur dan mengoptimalkan kinerja iklan dan menyajikan iklan yang lebih relevan atas nama kami atau perusahaan lain, termasuk di aplikasi dan situs web pihak ketiga.
  
   Pada umumnya, cookies akan dihapus saat Anda menutup browser Anda; ini disebut sebagai session cookies. Namun, cookies akan tetap berada pada perangkat browsing Anda baik sampai pada saat Anda menghapusnya atau aktivitas browsing berakhir. Dengan menggunakan Layanan ini, Anda setuju untuk Kami menggunakan data cookies. Data ini tidak mengungkapkan Informasi Pribadi apapun, dan dengan demikian Kami tidak mengumpulkan, memproses atau menggunakan Informasi Pribadi Anda.

   Anda dapat mengubah pengaturan browser komputer Anda untuk memblokir atau menghapus cookies. Jika Anda memilih untuk memblokir atau menghapus cookies Kami, Anda mungkin tidak dapat mengakses layanan Kami dan ini bisa berpengaruh pada kinerja Situs Kami di sistem Anda selaku Pengguna.
 
**5. UMUM**

   1. Penggunaan dan akses ke Situs ini diatur oleh Syarat dan Ketentuan serta Kebijakan Privasi Kami. Dengan mengakses atau menggunakan Situs ini, informasi, atau aplikasi lainnya dalam bentuk aplikasi mobile yang disediakan oleh atau dalam Situs, berarti Anda telah memahami dan menyetujui serta terikat dan tunduk dengan segala Syarat dan ketentuan yang berlaku di Situs ini.
 
   2. Kami berhak untuk menutup atau mengubah atau memperbaharui Kebijakan Privasi ini setiap saat tanpa pemberitahuan, dan berhak untuk membuat keputusan akhir jika tidak ada ketidakcocokan. Kami tidak bertanggung jawab atas kerugian dalam bentuk apapun yang timbul akibat perubahan pada Kebijakan Privasi. Pengguna dengan ini membebaskan Kami dari tanggungjawab atas tidak tersampaikannya data/ informasi yang disampaikan kepada Pengguna melalui berbagai jenis saluran komunikasi karena faktor kesalahan teknis yang tidak diduga-duga sebelumnya.
 
   3. Jika Anda masih memerlukan jawaban atas pertanyaan yang tidak terdapat dalam Kebijakan Privasi ini, Anda dapat menghubungi Kami di email itadmin@vivahealth.co.id atau menghubungi Kami di nomor 0800-1-503005 (bebas pulsa) selama jam operasional dari pukul 9.00 WIB sampai dengan 18.00 WIB.
 
   4. Selain itu Anda juga dapat membaca Frequently Asked Question (FAQ) yang telah kami siapkan untuk menjawab seputar pertanyaan-pertanyaan umum.

