---
title: Syarat dan Ketentuan
---

# Syarat dan Ketentuan

#### Selamat datang di Aplikasi Viva Apotek

Disarankan sebelum mengakses Situs ini lebih jauh, Pelanggan terlebih dahulu membaca dan memahami syarat dan ketentuan yang berlaku. Syarat dan ketentuan berikut adalah ketentuan dalam pengunjungan Situs, isi dan/atau konten, layanan, serta fitur lainnya yang ada di dalam Situs. Dengan mengakses atau menggunakan Situs atau aplikasi lainnya yang disediakan oleh atau dalam Situs, berarti Pelanggan telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.

1. **DEFINISI**

   Setiap kata atau istilah berikut yang digunakan di dalam Syarat dan Ketentuan ini memiliki arti seperti berikut di bawah, kecuali jika kata atau istilah yang bersangkutan di dalam pemakaiannya dengan tegas menentukan lain
  
   1.1. “PT Sumber Hidup Sehat atau Viva Health Indonesia” adalah pemilik dan pengelola Situs serta aplikasi mobile yang bernama Viva Apotek.
  
   1.2. “**Situs**”, berarti www.shop.vivahealth.co.id dan/atau aplikasi mobile Viva Apotek.
  
   1.3 “ Syarat dan Ketentuan” adalah perjanjian antara Pengguna dan aplikasi mobile Viva Apotek yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab Pengguna dan Viva Apotek.
   
   1.4  “Pengguna” adalah pihak yang menggunakan layanan Viva Apotek, termasuk namun tidak terbatas pada Pembeli maupun pihak lain yang sekedar berkunjung ke Situs Viva Apotek. 
  
   1.5 “**Pembeli**”, adalah Pengguna Belum Terdaftar dan Pengguna Terdaftar yang melakukan permintaan atas Produk yang dijual oleh Viva Apotek. 
  
   1.6 “Produk” adalah barang yang diperjualbelikan berdasarkan pada kategori yaitu Obat Bebas, Vitamin dan suplemen, Peralatan Medis, Perawatan tubuh dan kecantikan, Aksesoris, dan Makanan & Minuman Kesehatan. 
  
   1.7. “**Layanan**”, berarti setiap dan keseluruhan jasa serta informasi yang ada pada Situs, termasuk namun tidak terbatas pada informasi yang disediakan, fitur dan layanan aplikasi, layanan konsultasi dokter, dukungan data, serta aplikasi mobile yang Kami sediakan.
  
   1.8. “**Pihak Ketiga**”, berarti pihak lainya, termasuk namun tidak terbatas mitra penyedia layanan pembayaran, logistik atau kurir, infrastruktur situs dan mitra-mitra lainnya. 
  
   1.9. "**Informasi Pribadi**", merupakan tiap dan seluruh data pribadi yang diberikan oleh Pengguna di Situs Kami, termasuk namun tidak terbatas pada nama lengkap, tempat dan tanggal lahir, jenis kelamin, alamat, nomor identitas KTP, lokasi pengguna, kontak pengguna, serta dokumen dan data pendukung lainnya sebagaimana diminta pada ringkasan pendaftaran akun serta pada ringkasan aplikasi pengajuan.
  
   2.0. “**Konten**”, berarti teks, data, informasi, angka, gambar, grafik, foto, audio, video, nama pengguna, informasi, aplikasi, tautan, komentar, peringkat, desain, atau materi lainnya yang ditampilkan pada Situs.
 
2. **LAYANAN DAN/ATAU JASA**
   
   2.1. Informasi yang terdapat dalam Situs Kami ditampilkan sesuai keadaan kenyataan untuk tujuan informasi umum. Kami berusaha untuk selalu menyediakan dan menampilkan informasi yang terbaru dan akurat, namun Kami tidak menjamin bahwa segala informasi sesuai dengan ketepatan waktu atau relevansi dengan kebutuhan Pelanggan.
   
   2.2. Diharapkan Pelanggan tidak menganggap atau berasumsi bahwa Situs ini dapat dijadikan sebagai saran keuangan dan/atau finansial, atau rekomendasi atas produk serta layanan yang ditampilkan. Informasi di Situs ini disediakan untuk membantu Pelanggan dalam memilih produk atau layanan yang sesuai dengan kebutuhan Pelanggan. Pelanggan sepenuhnya bertanggung jawab atas keputusan terkait pemilihan produk dan layanan, atau dalam menPelanggantangani kontrak berkaitan dengan sebuah produk atau layanan. Dengan memberikan Kami rincian tentang diri pribadi Pelanggan, dianggap Pelanggan mengajukan diri untuk mendapatkan suatu produk atau layanan tertentu.
   
   2.3. Layanan yang Kami berikan tidak bersifat memaksa atau mengikat, serta tidak mengharuskan Pelanggan untuk mengikuti informasi yang tersedia. Tidak ada situasi atau keadaan apapun yang dapat membuat Kami dikenakan tanggung jawab atas kemungkinan kerugian yang Pelanggan alami karena pengambilan keputusan yang dilakukan oleh Pelanggan sendiri terkait tindakan atas informasi produk yang kami sampaikan di Situs.
   
   2.4. Kami memiliki hak untuk kapan saja menampilkan, mengubah, menghapus, menghilangkan, atau menambahkan segala konten yang ditampilkan dalam Situs ini. 
 
3. **PENGGUNAAN LAYANAN DAN JASA**
   
   Dengan Pelanggan melanjutkan penggunaan atau pengaksesan Situs ini, berarti Pelanggan telah menyatakan serta menjamin kepada Kami bahwa :

   3.1. Pelanggan hanya diperkenankan untuk mengakses dan/atau menggunakan Situs ini untuk keperluan pribadi dan non-komersil, yang berarti bahwa Situs ini hanya boleh diakses dan digunakan secara langsung oleh individu atau bisnis yang sedang mencari produk atau layanan untuk individu atau bisnis tersebut. Akses dan penggunaan Situs diluar dari keperluan pribadi atau non-komersil dilarang keras, dan melanggar Syarat dan Ketentuan ini.
   
   3.2. Pelanggan tidak diperkenankan menggunakan Situs dalam hal sebagai berikut :
    - Untuk menyakiti, menyiksa, mempermalukan, memfitnah, mencemarkan nama baik, mengancam, mengintimidasi atau mengganggu orang atau bisnis lain, atau apapun yang melanggar privasi atau yang Kami anggap cabul, menghina, penuh kebencian, tidak senonoh, tidak patut, tidak pantas, tidak dapat diterima, mendiskriminasikan atau merusak.
    - Dalam cara yang melawan hukum, tindakan penipuan atau tindakan komersil.
    - Melanggar atau menyalahi hak orang lain, termasuk tanpa kecuali : hak paten, merek dagang, hak cipta, rahasia dagang, publisitas, dan hak milik lainnya.
    - Untuk membuat, memeriksa, memperbarui, mengubah atau memperbaiki database, rekaman atau direktori Pelanggan ataupun orang lain.
    - Mengubah atau mengatur ulang bagian apapun dalam Situs ini yang akan mengganggu atau menaruh beban berlebihan pada sistem komunikasi dan teknis kami.
    - Mengunakan kode komputer otomatis, proses, program, robot, net crawler, spider, pemrosesan data, trawling atau kode komputer, proses, program atau sistem screen scraping alternatif.
    - Melanggar Syarat dan Ketentuan, atau petunjuk lainnya yang ada pada Situs.

   3.3. Kami tidak bertanggung jawab atas kehilangan akibat kegagalan mengakses Situs, dan metode penggunaan Situs yang diluar kendali Kami.
   
   3.4. Kami tidak bertanggung jawab atau dapat dipersalahkan atas kehilangan atau kerusakan yang diluar perkiraan saat Pelanggan mengakses atau menggunakan Situs ini. Ini termasuk kehilangan penghematan yang diharapkan, kehilangan bisnis atau kesempatan bisnis, kehilangan pemasukan atau keuntungan, atau kehilangan atau kerusakan apapun yang Pelanggan harus alami akibat penggunaan Situs ini.
 
4. **TRANSAKSI PEMBELIAN**
    
   Aturan tentang cara pembayaran bagaimana ?  yang saya draft ini apa sudah sesuai? Jika belum harap disesuaikan saja. 

   4.1 Saat melakukan pembelian Produk, Pembeli menyetujui bertanggung jawab untuk membaca, memahami, dan menyetujui informasi terkait dengan Produk yang akan dibelinya (termasuk didalamnya namun tidak terbatas pada merek, fungsi, kualitas, harga dan lainnya) sebelum melakukan pembelilan.

   4.2 Pembayaran oleh Pembeli wajib dilakukan segera (selambat-lambatnya dalam batas waktu 1x24 jam) setelah Pembeli melakukan check- out. Jika dalam batas waktu tersebut Pembeli belum melakukan pembayaran, maka Kami memiliki kewenangan untuk membatalkan transaksi tersebut. Pembeli tidak berhak mengajukan klaim atau tuntutan atas pembatalan tersebut.  

   4.3 Pembeli wajib bertransaksi melalui prosedur yang telah ditetapkan oleh Viva Apotek. Pembeli melakukan pembayaran dengan menggunakan metode pembayaran transfer melalui virtual account. 

   4.4 Pembeli memahami dan menyetujui bahwa keterlambatan pengiriman Produk adalah bukan merupakan tanggung jawab dari Viva Apotek. 
 
5. **HAK INTELEKTUAL PROPERTI**
   
   Semua Hak Kekayaan Intelektual yang ada di dalam Situs ini adalah milik Kami. Tiap atau keseluruhan informasi dan materi dan konten, termasuk namun tidak terbatas pada tulisan, perangkat lunak, teks, data, grafik, gambar, audio, video, logo, ikon atau kode-kode html dan kode-kode lain yang ada di Situs ini dilarang dipublikasikan, dimodifikasi, disalin, digPelanggankan atau diubah dengan cara apapun di luar area Situs ini tanpa izin dari Kami. Pelanggaran terhadap hak-hak Situs ini dapat ditindak sesuai dengan peraturan hukum yang berlaku di Indonesia. 

   Pengguna dapat menggunakan informasi atau isi dalam Situs hanya untuk penggunaan pribadi non-komersil. Kecuali ditentukan sebaliknya dan/atau diperbolehkan secara tegas oleh undang-undang hak cipta, maka Pengguna dilarang untuk menyalin, membagikan ulang, mentransmisi ulang, meretas, mempublikasi atau melakukan tindakan eksploitasi komersial dari pengunduhan yang dilakukan tanpa seizin Kami sebagai pemilik Hak Intelektual Properti tersebut. Dalam hal Pengguna telah mendapatkan izin yang diperlukan maka Pengguna dilarang melakukan perubahan atau penghapusan. Pengguna dengan ini menyatakan menerima dan mengetahui bahwa dengan mengunduh materi Hak Intelektual Properti bukan berarti mendapatkan hak kepemilikan atas pengunduhan materi Hak Intelektual Properti tersebut.
 
6. **GANTI RUGI**

   Pengguna akan melepaskan Viva Apotek dari tuntutan ganti rugi dan menjaga nama baik dari Viva Apotek dari setiap klaim atau tuntutan, termasuk biaya hukum yang dilakukan pihak ketiga yang timbul dalam hal Pelanggan melanggar Perjanjian ini, 

7. **KOMENTAR**

   Jika Pengguna ingin memberikan komentar, masukan, ataupun sanggahan mengenai tiap atau keseluruhan konten dan informasi dalam Situs, Kami juga menyediakan sarana chat/messaging, kontak email, blog, ataupun media sosial milik Kami. Pelanggan diperbolehkan untuk memberi komentar, dan setuju bahwa jika diperlukan, akan Kami pergunakan untuk kepentingan informasi dan ditampilkan pada Situs Kami. Dan harap diperhatikan bahwa komentar, masukan, atau sanggahan yang Pelanggan berikan tidak boleh bertentangan dengan Syarat dan Ketentuan ini, terutama seperti tercantum dalam poin 3.2.
 
8. **PENGAJUAN (apa kita masuk kategori ini?)**

   8.1. Pelanggan bisa mengajukan diri untuk mendapatkan berbagai produk dan layanan seperti yang ditampilkan di Situs ini. Produk dan layanan tersebut disediakan oleh Pihak Ketiga. Pelanggan sepenuhnya bertanggung jawab atas pilihan produk atau layanan, dan Kami tidak bertanggung jawab atau dapat dipersalahkan atas kehilangan atau kerusakan yang mungkin Pelanggan derita akibat produk atau layanan yang Pelanggan dapatkan saat menggunakan Situs ini, atau atas tindakan, penghapusan atau kesalahan Pihak Ketiga yang disebut di produk atau layanan itu.

   8.2. Diingatkan kembali bahwa data dan deskripsi produk dan jasa pada Situs ini mungkin tidak mewakili seluruh deskripsi dari semua pilihan dan persyaratan produk dan layanan tersebut. Pelanggan perlu berhati-hati dan cermat untuk melihat semua pilihan dan syarat dan kondisi dari setiap produk atau jasa sebelum melakukan pengajuan.

   8.3. Jika Pelanggan mengajukan permohonan dan mendapatkan produk atau layanan, Pelanggan akan masuk ke dalam kontak dengan Pihak Ketiga sebagai penyedia produk atau layanan, yang memiliki syarat dan ketentuan sendiri. Pelanggan bertanggung jawab untuk memastikan bahwa Pelanggan setuju dengan syarat dan kondisi sebelum memasuki kontrak untuk mendapatkan produk atau layanan dimaksud. Kami tidak bertanggung jawab atau disalahkan atas segala kerugian atau kerusakan yang mungkin Pelanggan derita akibat syarat dan ketentuan yang berlaku di setiap kontrak antara Pelanggan dengan Pihak Ketiga, termasuk yang berhubungan tindakan, kelalaian atau kesalahan dari Pihak Ketiga.

   8.4. Pelanggan memberikan izin pada Kami untuk memproses data-data Pelanggan kepada Pihak Ketiga, serta memberi kuasa kepada Pihak Ketiga untuk memeriksa semua informasi yang diberikan dengan cara bagaimanapun, menghubungi Pelanggan untuk pemasaran dari produk maupun layanan yang disediakan oleh Pihak Ketiga pada Situs, termasuk namun tidak terbatas untuk keperluan Sistem Layanan Informasi Keuangan (SLIK) OJK, biro kredit dan/atau sejenisnya.

   8.5. Pelanggan memberikan izin pada Kami untuk memproses data Pelanggan untuk meningkatkan layanan Kami dalam bentuk apapun, termasuk tapi tidak terbatas untuk keperluan pemasaran dan peningkatan layanan Kami.
 
9. **TAUTAN LAINNYA**

   Situs Kami mungkin menyediakan beberapa tautan tertentu yang merujuk atau mengarahkan Pelanggan ke laman dari situs Pihak Ketiga, juga termasuk di dalamnya tautan yang tersedia pada halaman hasil pencarian produk tertentu. Pelanggan mengetahui, memahami, serta menyetujui bahwa Kami tidak bertanggung jawab atas konten dan/atau materi lainnya yang terdapat dalam tautan milik Pihak Ketiga tersebut. Pelanggan bertanggung jawab atas resiko ketika mengakses dan menggunakan tautan milik Pihak Ketiga tersebut. Dan disarankan Pelanggan juga dapat membaca serta memahami isi dari Syarat dan Ketentuan yang berlaku di tautan milik Pihak Ketiga tersebut.
 
10. **ATURAN KEANGGOTAAN**

    Selain menampilkan informasi produk-produk kesehatan serta jasa konsultasi dokter, Kami juga menyediakan keanggotaan bagi Pelanggan selaku Pengguna. Pelanggan dapat memeriksa riwayat kesehatan Pelanggan dan juga menemukan produk yang sesuai dengan profil Pelanggan.

    10.1. Sebelum menjadi anggota (atau disebut dengan Pengguna Terdaftar), Pelanggan diwajibkan membuat akun untuk diri Pelanggan. Proses pembuatan akun dapat dilakukan secara online melalui Situs.

    10.2. Sebelum menjadi Pengguna Terdaftar, Pelanggan diwajibkan membaca, memahami, dan mematuhi Syarat dan Ketentuan serta Kebijakan Privasi yang berlaku di Situs.

    10.3. Pelanggan diharuskan memberi data Informasi Pribadi yang sebenarnya dan tidak memberikan informasi menyimpang dan/atau informasi yang tidak relevan dalam melakukan proses registrasi menjadi Pengguna Terdaftar. Selain itu Pelanggan juga diharuskan memberi kontak detail yang benar dan valid.

    10.4. Kami berhak untuk tidak memproses registrasi Pengguna yang tidak memenuhi persyaratan ataupun tidak memberikan informasi yang benar dan valid, dan Kami berhak juga mengeluarkan Pengguna Terdaftar dari keanggotaan jika di kemudian hari ditemukan hal-hal yang melanggar ketentuan dari Kami.

    10.5. Pengguna yang telah sukses melakukan proses registrasi, dan menjadi Pengguna Terdaftar, dapat melakukan akses keanggotaan kapan pun melalui Situs ataupun aplikasi mobile milik Kami.

    10.6. Semua Pengguna Terdaftar wajib mematuhi Syarat dan Ketentuan yang berlaku di Situs ini, dan tidak diperkenankan melakukan tindakan-tindakan seperti tertuang dalam poin 3.2, dalam keanggotaan Situs.
 
11. **UMUM**

    11.1. Penggunaan dan akses ke Situs ini diatur oleh Syarat dan Ketentuan serta Kebijakan Privasi Kami. Dengan mengakses atau menggunakan Situs ini, informasi, atau aplikasi lainnya dalam bentuk aplikasi mobile yang disediakan dalam Situs, berarti Pelanggan telah memahami, menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.
   
    11.2. Kami berhak untuk menutup atau mengubah atau memperbaharui Syarat dan Ketentuan ini setiap saat tanpa pemberitahuan, dan berhak untuk membuat keputusan akhir jika tidak ada ketidakcocokan. Kami tidak bertanggung jawab atas kerugian dalam bentuk apa pun yang timbul akibat perubahan pada Syarat dan Ketentuan.
   
    11.3. Jika Pelanggan masih memerlukan jawaban atas pertanyaan yang tidak terdapat dalam Syarat dan Ketentuan ini, Pelanggan dapat menghubungi Kami di email itadmin@vivahealth.co.id atau menghubungi Kami di nomor 0800-1-503005 (bebas pulsa) selama jam operasional dari pukul 9.00 WIB sampai dengan 18.00 WIB.
   
    11.4. Selain itu Pelanggan juga dapat membaca Frequently Asked Question (FAQ) yang telah kami siapkan untuk menjawab seputar pertanyaan-pertanyaan umum.
