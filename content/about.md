---
title: About us
---

# Tentang Kami
####   Selamat datang di Viva Health
Viva Health adalah penyedia layanan kesehatan terpadu yang didirikan di tahun 2012, dan kini telah beroperasi di Jawa Barat (Bekasi), Jawa Tengah, D.I. Yogyakarta, Jawa Timur dan Bali. Saat ini kami memiliki 145 cabang Apotek, dimana lebih dari 50% lokasi kami telah dilengkapi dengan Praktik Dokter Umum. Dengan dukungan yang diberikan lebih dari 1000 karyawan, setiap tahunnya kami melayani 5 juta pelanggan mulai dari kebutuhan obat dan produk kesehatan hingga pengecekan kesehatan.

Sesuai dengan Tekad Kami, Viva Health hadir untuk membantu Anda untuk hidup lebih lama dalam kondisi sehat, yang didukung oleh para tenaga medis profesional sebagai mitra kami.

Konsultasikan kesehatan Anda kepada Dokter dan Apoteker kami, karena "Sehat itu Pilihan".

Semoga Anda dan keluarga tetap sehat selalu.

# Tekad Kami
#### Kami bertanggung jawab atas kesehatan masyarakat disekitar kami.

Sebagai bagian dari masyarakat, kami bertekad untuk selalu peduli akan kesehatan masyarakat dengan sepenuh hati. Kami akan terus-menerus meningkatkan pengetahuan, agar kami dapat membantu mereka melakukan hal yang tepat untuk menjaga kesehatan mereka.

Semua ini kami lakukan agar mereka dapat menjalani kehidupan yang lebih sehat secara optimal.