import VuexPersistence from 'vuex-persist';

export default ({store}) => {
  new VuexPersistence({
    reducer: (state) => {
      return {cart: state.cart};
    },
  }).plugin(store);
};
