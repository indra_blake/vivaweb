import Vue from 'vue';
import VueGtag from 'vue-gtag';

export default ({app}) => {
  // const gtagId = process.env.GOOGLE_ANALYTICS_TAG_ID;

  Vue.use(
    VueGtag,
    {
      config: {id: 'G-3J4H44824W'}, // production is G-3J4H44824W
      onReady() {},
      onBeforeTrack() {},
      onAfterTrack() {},
    },
    app.router,
  );
};
