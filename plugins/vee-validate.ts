import Vue from 'vue';
import {
  extend,
  localize,
  ValidationObserver,
  ValidationProvider,
} from 'vee-validate';

import * as rules from 'vee-validate/dist/rules';
import en from 'vee-validate/dist/locale/en.json';
import id from 'vee-validate/dist/locale/id.json';
import {ValidationRule} from 'vee-validate/dist/types/types';
import PhoneNumber from 'awesome-phonenumber';

localize({
  en,
  id,
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

for (const [rule, validation] of Object.entries(rules)) {
  extend(rule, <ValidationRule>{
    ...validation,
  });
}

extend('phone', {
  message(fieldName) {
    return `${fieldName} bukan nomor telepon yang valid`;
  },
  validate(value) {
    return new Promise((resolve) => {
      const phone = new PhoneNumber(value, 'id');
      resolve({valid: phone.isValid()});
    });
  },
});

extend('decimal', {
  validate: (value, {decimals = '*', separator = '.'} = {}) => {
    if (value === null || value === undefined || value === '') {
      return {
        valid: false,
      };
    }
    if (Number(decimals) === 0) {
      return {
        valid: /^-?\d*$/.test(value),
      };
    }
    const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
    const regex = new RegExp(
      `^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`,
    );

    return {
      valid: regex.test(value),
    };
  },
  message: 'The {_field_} field must contain only decimal values',
});

export default ({app}) => {
  localize(app.i18n.locale);
  const beforeLanguageSwitch = app.i18n.beforeLanguageSwitch;
  app.i18n.beforeLanguageSwitch = (oldLocale, newLocale) => {
    const res = beforeLanguageSwitch(oldLocale, newLocale);
    localize(newLocale);
    return res;
  };
};
