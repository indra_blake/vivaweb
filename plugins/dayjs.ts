export default ({app}, inject) => {
  const {$dayjs} = app;
  app.i18n.onLanguageSwitched = () => {
    $dayjs.locale(app.i18n.locale);
  };
};
