import QiscusSDK from 'qiscus-sdk-core';

export default (function QiscusStoreSingleton(context, inject) {
  const qiscus = new QiscusSDK();

  qiscus.init({
    // change this into your own AppId through https://dashboard.qiscus.com
    AppId: process.env.QISCUS_APP_ID
      ? process.env.QISCUS_APP_ID
      : 'myviva-ynhp0cwdqpkdgq',
    options: {
      async loginSuccessCallback() {
        await context.store.dispatch('message/getRooms');
        const dataRoom = {...context.store.state.message.rooms};
        // await context.store.dispatch('consultation/getConsultations');
        await context.store.dispatch(
          'message/getChatRooms',
          Object.keys(dataRoom).map((val) => dataRoom[val].room_id),
        );
      },
      async newMessagesCallback(data) {
        await context.store.dispatch('message/getNewMessage', data[0]);
        if (
          data[0].payload !== null &&
          Object.keys(data[0].payload).length > 0
        ) {
          if (
            ['doctor_end_chat', 'Thank you for consulting'].includes(
              data[0].message,
            )
          ) {
            await context.store.dispatch('consultation/getConsultations', {
              'filter[room_status]': 'active',
            });
            await context.store.dispatch(
              'consultation/getDetail',
              data[0].payload.content.consultation_id,
            );
          }
        }
        await context.store.dispatch('message/getRooms');
        const dataRoom = {...context.store.state.message.rooms};
        await context.store.dispatch(
          'message/getChatRooms',
          Object.keys(dataRoom).map((val) => dataRoom[val].room_id),
        );
      },
      commentDeliveredCallback(data) {
        context.store.dispatch('message/getDeliveredMessage', data.comment);
      },
      commentReadCallback(data) {
        context.store.dispatch('message/getReadMessage', data.comment);
      },
    },
  });
  inject('qiscus', qiscus);
});
