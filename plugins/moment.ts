export default ({app}, inject) => {
  const {$moment} = app;
  app.i18n.onLanguageSwitched = () => {
    $moment.locale(app.i18n.locale);
  };
};
