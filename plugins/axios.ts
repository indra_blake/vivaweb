export default ({$axios}) => {
  $axios.onRequest((config) => {
    config.headers.common['Client-Version'] = '1.0.3.7';
  });
};
