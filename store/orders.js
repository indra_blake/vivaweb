import {namespace} from 'vuex-class';

export const orderModule = namespace('orders');

export const SET_ORDERS = 'SET_ORDERS';
export const SET_DETAIL_ORDER = 'SET_DETAIL_ORDER';
export const SET_META_ORDERS = 'SET_META_ORDERS';
export const SET_STATUS = 'SET_STATUS';
export const SET_TRACKING = 'SET_TRACKING';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  orders: [],
  detail: {},
  meta: {},
  status: {},
  tracking: [],
});

export const mutations = {
  [SET_ORDERS](state, {data}) {
    state.orders = data;
  },
  [SET_DETAIL_ORDER](state, {data}) {
    state.detail = data;
  },
  [SET_META_ORDERS](state, {data}) {
    state.meta = data;
  },
  [SET_STATUS](state, {data}) {
    state.status = data;
  },
  [SET_TRACKING](state, {data}) {
    state.tracking = data;
  },
};

export const actions = {
  async getOrders({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/my/order?${filter}`);

      commit(SET_ORDERS, {
        data: res.data.data.data,
      });

      commit(SET_META_ORDERS, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDetailOrder({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/my/order/${id}`);

      commit(SET_DETAIL_ORDER, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getStatus({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/status?${filter}`);

      commit(SET_STATUS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getTrackingOrder({commit}, params = {}) {
    try {
      const res = await this.$axios.$post(
        `/api/shipping/anteraja/tracking`,
        params,
      );
      commit(SET_TRACKING, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
};
