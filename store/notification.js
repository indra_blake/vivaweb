import {namespace} from 'vuex-class';

export const notificationModule = namespace('notification');

export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';

export const SET_COUNTER = 'SET_COUNTER';

export const SET_CAMPAIGN = 'SET_CAMPAIGN';

export const state = () => ({
  counter: 0,
  notifications: [],
  campaign: [],
});

export const mutations = {
  [SET_NOTIFICATIONS](state, {data}) {
    state.notifications = data;
  },
  [SET_COUNTER](state, counter) {
    state.counter = counter;
  },
  [SET_CAMPAIGN](state, counter) {
    state.campaign = counter;
  },
};

export const actions = {
  async getNotifications({commit}, params = {}) {
    try {
      const res = await this.$axios.get('/api/my/notifications', {params});
      commit(SET_NOTIFICATIONS, {
        data: res.data.data.data,
      });
      commit(SET_COUNTER, res.data.data.total);
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getNotificationDetail({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/campaigns/${id}`);
      commit(SET_CAMPAIGN, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
