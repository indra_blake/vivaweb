import {namespace} from 'vuex-class';

export const promotionModule = namespace('promotion');

export const SET_PROMOTIONS = 'SET_PROMOTIONS';
export const SET_PROMOTION = 'SET_PROMOTION';

export const state = () => ({
  promotions: [],
  promotion: {},
});

export const mutations = {
  [SET_PROMOTIONS](state, {data}) {
    state.promotions = data;
  },
  [SET_PROMOTION](state, data) {
    state.promotion = data;
  },
};

export const actions = {
  async getPromotions({commit}, params = {}) {
    try {
      const res = await this.$axios.$get(`/api/promo`, {params});

      commit(SET_PROMOTIONS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
};
