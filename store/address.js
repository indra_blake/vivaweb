import {namespace} from 'vuex-class';

export const addressModule = namespace('address');

export const SET_ADDRESS = 'SET_ADDRESS';

export const SET_DETAIL_ADDRESS = 'SET_DETAIL_ADDRESS';

export const SET_DEFAULT_ADDRESS = 'SET_DEFAULT_ADDRESS';

export const SET_META_ADDRESS = 'SET_META_ADDRESS';

export const SET_PROVINCE = 'SET_PROVINCE';

export const SET_CITY = 'SET_CITY';

export const SET_DISTRICT = 'SET_DISTRICT';

export const SET_VILLAGE = 'SET_VILLAGE';

export const ADD_ADDRESS = 'ADD_ADDRESS';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  address: [],
  default: null,
  detail: {},
  meta: {},
  province: [],
  city: [],
  district: [],
  village: [],
});

export const mutations = {
  [SET_ADDRESS](state, {data}) {
    state.address = data;
  },
  [SET_DETAIL_ADDRESS](state, {data}) {
    state.detail = data;
  },
  [SET_DEFAULT_ADDRESS](state, {data}) {
    state.default = data;
  },
  [SET_META_ADDRESS](state, {data}) {
    state.meta = data;
  },
  [SET_PROVINCE](state, {data}) {
    state.province = data;
  },
  [SET_CITY](state, {data}) {
    state.city = data;
  },
  [SET_DISTRICT](state, {data}) {
    state.district = data;
  },
  [SET_VILLAGE](state, {data}) {
    state.village = data;
  },
  [ADD_ADDRESS](state, {data}) {
    state.address = [...state.address, data];
  },
};

export const actions = {
  async getAddress({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/my/address?${filter}`);

      const addressClone = [...res.data.data.data];
      const defaultAddress = addressClone.find((val) => {
        return val.default_address === 1;
      });
      commit(SET_ADDRESS, {
        data: res.data.data.data,
      });
      commit(SET_DEFAULT_ADDRESS, {
        data: defaultAddress !== undefined ? defaultAddress.id : undefined,
      });

      commit(SET_META_ADDRESS, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getProvince({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/provinces?${filter}`);

      commit(SET_PROVINCE, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getCity({commit}, provinceId) {
    try {
      const res = await this.$axios.get(`/api/provinces/${provinceId}/cities`);

      commit(SET_CITY, {
        data: res.data.data.cities,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDistrict({commit}, cityId) {
    try {
      const res = await this.$axios.get(`/api/cities/${cityId}/districts`);

      commit(SET_DISTRICT, {
        data: res.data.data.districts,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getVillage({commit}, districtId) {
    try {
      const res = await this.$axios.get(
        `/api/districts/${districtId}/villages`,
      );

      commit(SET_VILLAGE, {
        data: res.data.data.villages,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async saveAddress({commit}, params = {}) {
    try {
      const res = await this.$axios.post(`/api/my/address`, params);
      // commit(ADD_ADDRESS, {
      //   data: res.data.data,
      // });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async deleteAddress({commit}, id) {
    try {
      const res = await this.$axios.delete(`api/my/address/${id}`);
      // commit(ADD_ADDRESS, {
      //   data: res.data.data,
      // });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDetailAddress({commit}, addressId) {
    try {
      const res = await this.$axios.get(`/api/my/address/${addressId}`);

      commit(SET_DETAIL_ADDRESS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async updateAddress({commit}, params = {}) {
    try {
      const res = await this.$axios.put(
        `/api/my/address/${params.id}`,
        params.form,
      );
      // commit(ADD_ADDRESS, {
      //   data: res.data.data,
      // });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async setDefaultAddress({commit}, id) {
    try {
      const res = await this.$axios.put(`/api/my/address/${id}/default`);
      // commit(ADD_ADDRESS, {
      //   data: res.data.data,
      // });
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
