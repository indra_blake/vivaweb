export const strict = false;

export const state = () => ({
  counter: 0,
  isDark: false,
});

export const mutations = {
  setDark(state, value) {
    state.isDark = value;
  },
  increment(state) {
    state.counter++;
  },
  decrement(state) {
    state.counter--;
  },
};
