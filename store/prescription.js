import {namespace} from 'vuex-class';

export const prescriptionModule = namespace('prescription');

export const SET_PRESCRIPTION = 'SET_PRESCRIPTION';

export const SET_DETAIL_PRESCRIPTION = 'SET_DETAIL_PRESCRIPTION';

export const SET_META_PRESCRIPTION = 'SET_META_PRESCRIPTION';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  prescriptions: [],
  detail: {},
  meta: {},
});

export const mutations = {
  [SET_PRESCRIPTION](state, {data}) {
    state.prescriptions = data;
  },

  [SET_DETAIL_PRESCRIPTION](state, {data}) {
    state.detail = data;
  },

  [SET_META_PRESCRIPTION](state, {data}) {
    state.meta = data;
  },
};

export const actions = {
  async getPrescriptions({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/prescriptions?${filter}`);
      commit(SET_PRESCRIPTION, {
        data: res.data.data.data,
      });

      commit(SET_META_PRESCRIPTION, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async createPrescription({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/prescriptions', params);
      commit(SET_DETAIL_PRESCRIPTION, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDetailPrescription({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/prescriptions/${id}`);
      commit(SET_DETAIL_PRESCRIPTION, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async prescriptionToCart({commit}, id) {
    try {
      const res = await this.$axios.$post(
        `api/prescriptions/${id}/add-to-cart`,
      );
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
