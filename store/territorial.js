import {namespace} from 'vuex-class';

export const territorialModule = namespace('territorial');

const SET_CITIES = 'SET_CITIES';

export const state = () => ({
  cities: [],
});

export const mutations = {
  [SET_CITIES](state, cities) {
    state.cities = cities;
  },
};

export const actions = {
  async getCities({commit}, params = {}) {
    try {
      const res = await this.$axios.get('/api/cities', {params});
      commit(SET_CITIES, res.data.data.data);
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
