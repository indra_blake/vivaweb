import {namespace} from 'vuex-class';

export const reviewModule = namespace('review');

export const SET_REVIEWS = 'SET_REVIEWS';
export const SET_REVIEW = 'SET_REVIEW';

export const state = () => ({
  reviews: [],
  review: {},
});

export const mutations = {
  [SET_REVIEWS](state, {data}) {
    state.reviews = data;
  },
  [SET_REVIEW](state, {data}) {
    state.review = data;
  },
};

export const actions = {
  async getDetail({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/consultation/review/${id}`);
      commit(SET_REVIEW, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async createReview({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/consultation/review', params);
      commit(SET_REVIEW, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
