import {namespace} from 'vuex-class';

export const socialModule = namespace('socials');

export const SET_SOCIAL = 'SET_SOCIAL';

export const state = () => ({
  socials: [],
});

export const mutations = {
  [SET_SOCIAL](state, {data}) {
    state.socials = data;
  },
};

export const actions = {
  async getSocial({commit}, params = {}) {
    try {
      const res = await this.$axios.get('/api/social-networks', {params});
      commit(SET_SOCIAL, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
