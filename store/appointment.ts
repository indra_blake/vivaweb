import {namespace} from 'vuex-class';

export const appointmentModule = namespace('appointment');

export const SET_APPOINTMENTS = 'SET_APPOINTMENTS';
export const SET_APPOINTMENT = 'SET_APPOINTMENT';

export const state = () => ({
  appointments: [],
  appointment: {},
});

export const mutations = {
  [SET_APPOINTMENTS](state, {data}) {
    state.appointments = data;
  },
  [SET_APPOINTMENT](state, data) {
    state.appointment = data;
  },
};

export const actions = {
  async getAppointments({commit}, params = {}) {
    try {
      const res = await this.$axios.$get(`/api/appointment`, {params});

      commit(SET_APPOINTMENTS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetail({commit}, id: number) {
    try {
      const res = await this.$axios.$get(`/api/appointment/${id}`);
      commit(SET_APPOINTMENT, res.data);
      return res;
    } catch (e) {
      return e.response.data;
    }
  },

  async createAppointment({commit}, params: any = {}) {
    try {
      const res = await this.$axios.$post(`/api/appointment`, params);
      commit(SET_APPOINTMENT, res.data);
      return res;
    } catch (e) {
      return e.response.data;
    }
  },
  async purchaseAppointment({commit}, params: any = {}) {
    try {
      const res = await this.$axios.$post(`/api/appointment/purchase`, params);
      commit(SET_APPOINTMENT, res.data);
      return res;
    } catch (e) {
      return e.response.data;
    }
  },
  async approveAppointment({commit}, id: number | string) {
    try {
      const res = await this.$axios.$post(`/api/appointment/${id}/approve`);
      commit(SET_APPOINTMENT, res.data);
      return res;
    } catch (e) {
      return e.response.data;
    }
  },
  async rejectAppointment({commit}, id: number | string) {
    try {
      const res = await this.$axios.$post(`/api/appointment/${id}/cancel`);
      commit(SET_APPOINTMENT, res.data);
      return res;
    } catch (e) {
      return e.response.data;
    }
  },
};
