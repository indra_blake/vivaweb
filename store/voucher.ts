import {namespace} from 'vuex-class';

export const voucherModule = namespace('voucher');

export const SET_VOUCHERS = 'SET_VOUCHERS';
export const SET_VOUCHER = 'SET_VOUCHER';

export interface Voucher {
  [key: string]: any;
}

export const state = () => ({
  vouchers: [],
  voucher: {},
});

export const mutations = {
  [SET_VOUCHERS](state, {data}) {
    state.vouchers = data;
  },
  [SET_VOUCHER](state, data) {
    state.voucher = data;
  },
};

export const actions = {
  async getListVoucher({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/voucher`, {params});

      commit(SET_VOUCHERS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetailVoucher({commit}, id: number | string) {
    try {
      const res = await this.$axios.get(`/api/voucher/${id}`);

      commit(SET_VOUCHER, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postUseVoucher({commit}, {id, params = {}}) {
    try {
      const res = await this.$axios.get(`/api/order/${id}/apply-voucher`, {
        params,
      });

      commit(SET_VOUCHERS, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
};
