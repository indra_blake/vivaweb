import {namespace} from 'vuex-class';

export const uploadModule = namespace('upload');

export const SET_FILE = 'SET_FILE';

export const state = () => ({
  files: {},
});

export const mutations = {
  [SET_FILE](state, files) {
    state.files = files;
  },
};

export const actions = {
  async uploadFile({commit}, file) {
    try {
      const res = await this.$axios.post('/api/files', file, {
        'Content-Type': 'multipart/form-data',
      });
      commit(SET_FILE, res.data.data);
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
