import {namespace} from 'vuex-class';

export const bannersModule = namespace('banners');

const SET_BANNERS = 'SET_BANNERS';

export const state = () => ({
  banners: [],
});

export const mutations = {
  [SET_BANNERS](state, banners) {
    state.banners = banners;
  },
};

export const actions = {
  async all({commit}, params = {}) {
    try {
      const res = await this.$axios.get('/api/banners', {params});
      commit(SET_BANNERS, res.data.data.data);
      return res;
    } catch (e) {
      return e.response;
    }
  },
};

export const getters = {
  banners: (state) => state.banners.filter((item) => !!item.img_url),
};
