import {namespace} from 'vuex-class';

export const pharmaciesModule = namespace('pharmacies');

export const SET_PHARMACIES = 'SET_PHARMACIES';

export const SET_DETAIL_PHARMACY = 'SET_DETAIL_PHARMACY';

export const SET_NEAREST_PHARMACY = 'SET_NEAREST_PHARMACY';

export const SET_RECOMMENDED_PHARMACY = 'SET_RECOMMENDED_PHARMACY';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  pharmacies: [],
  detail: {},
  nearest: [],
  recommended: [],
});

export const mutations = {
  [SET_PHARMACIES](state, {data}) {
    state.pharmacies = data;
  },

  [SET_DETAIL_PHARMACY](state, {data}) {
    state.detail = data;
  },
  [SET_NEAREST_PHARMACY](state, {data}) {
    state.nearest = data;
  },
  [SET_RECOMMENDED_PHARMACY](state, {data}) {
    state.recommended = data;
  },
};

export const actions = {
  async getPharmacies({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/pharmacies?perPage=999${filter}`);
      commit(SET_PHARMACIES, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetailPharmacy({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/pharmacies/${id}`);
      commit(SET_DETAIL_PHARMACY, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getNearestPharmacy({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/pharmacies/nearest?${filter}`);
      commit(SET_NEAREST_PHARMACY, {
        data: res.data.data ? res.data.data : [],
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getRecommendedPharmacy({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(
        `/api/pharmacies/recommended?${filter}`,
      );
      commit(SET_NEAREST_PHARMACY, {
        data: res.data.data ? res.data.data : [],
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getRecommendedPharmacyItem({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(
        `/api/pharmacies/recommended?${filter}`,
      );
      commit(SET_RECOMMENDED_PHARMACY, {
        data: res.data.data ? res.data.data : [],
      });
      return res;
    } catch (e) {
      commit(SET_RECOMMENDED_PHARMACY, {
        data: [],
      });
      return e.response;
    }
  },
};
