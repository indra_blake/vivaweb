import {namespace} from 'vuex-class';

export const productModule = namespace('products');

export const SET_PRODUCT = 'SET_PRODUCT';

export const SET_PROMOTED_PRODUCT = 'SET_PROMOTED_PRODUCT';

export const SET_PROMO_CHECK = 'SET_PROMO_CHECK';

export const SET_DETAIL_PRODUCT = 'SET_DETAIL_PRODUCT';

export const SET_SKU_DETAIL_PRODUCT = 'SET_SKU_DETAIL_PRODUCT';

export const SET_CATEGORIES = 'SET_CATEGORIES';

export const SET_PRODUCT_META = 'SET_PRODUCT_META';

export const SET_RELATED_PRODUCT = 'SET_RELATED_PRODUCT';

export const SET_SUB_CATEGORIES = 'SET_SUB_CATEGORIES';

export const SET_SUGESTION = 'SET_SUGESTION';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  products: [],
  promoted: [],
  detail: {},
  categories: [],
  meta: {},
  promotion: {},
  sugestion: [],
  related: [],
  subcategories: [],
});

export const mutations = {
  [SET_PRODUCT](state, {data}) {
    state.products = data;
  },
  [SET_PROMOTED_PRODUCT](state, {data}) {
    state.promoted = data;
  },
  [SET_DETAIL_PRODUCT](state, {data}) {
    state.detail = data;
  },
  [SET_CATEGORIES](state, {data}) {
    state.categories = data;
  },
  [SET_SKU_DETAIL_PRODUCT](state, {data}) {
    state.detail = data;
  },
  [SET_PRODUCT_META](state, {data}) {
    state.meta = data;
  },
  [SET_PROMO_CHECK](state, {data}) {
    state.promotion = data;
  },
  [SET_RELATED_PRODUCT](state, {data}) {
    state.related = data;
  },
  [SET_SUB_CATEGORIES](state, {data}) {
    state.subcategories = data;
  },
  [SET_SUGESTION](state, {data}) {
    state.sugestion = data;
  },
};

export const actions = {
  async getProducts({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/products?${filter}`);
      commit(SET_PRODUCT, {
        data: res.data.data.data,
      });

      commit(SET_PRODUCT_META, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetailProduct({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/products/${id}`);
      commit(SET_DETAIL_PRODUCT, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDetailProductBySku({commit}, sku) {
    try {
      const res = await this.$axios.get(`/api/products/sku/${sku}`);
      commit(SET_SKU_DETAIL_PRODUCT, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getSugestion({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/products/search-suggestion`, {
        params,
      });
      commit(SET_SUGESTION, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getPromotedProduct({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/products/promoted?${filter}`);
      commit(SET_PROMOTED_PRODUCT, {
        data: res.data.data.data,
      });
      commit(SET_PRODUCT_META, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getPromoCheck({commit}, promoName) {
    try {
      const res = await this.$axios.get(`/api/promotions/${promoName}/check`);
      commit(SET_PROMO_CHECK, {
        data: {
          name: promoName,
          valid: res.data.data.is_valid,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getCategories({commit}, params = {}) {
    try {
      let filter = [];
      if (Object.keys(params).length !== 0) {
        filter = Object.keys(params)
          .map((val) => {
            return `filter[${val}]=${params[val]}`;
          })
          .join('&');
      }

      const res = await this.$axios.get(`/api/product-categories?${filter}`);
      commit(SET_CATEGORIES, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getSubCategories({commit}, id) {
    try {
      const res = await this.$axios.get(
        `/api/product-categories/${id}/sub-categories`,
      );
      commit(SET_SUB_CATEGORIES, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getSubSegment({commit}, id) {
    try {
      const res = await this.$axios.get(
        `/api/product-categories/${id}/segments`,
      );
      commit(SET_SUB_CATEGORIES, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getRelatedProduct({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/products/${id}/related`);
      commit(SET_RELATED_PRODUCT, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line no-empty-pattern
  async buyProduct({}, id) {
    try {
      const res = await this.$axios.$post(`/api/products/${id}/buy`);
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
