import {namespace} from 'vuex-class';

export const snackbarModule = namespace('snackbar');

export const SET_SNACKBARS = 'SET_SNACKBARS';

export const state = () => ({
  snackbar: {},
});

export const mutations = {
  [SET_SNACKBARS](state, payload) {
    state.snackbar = payload;
  },
};

export const actions = {
  setSnackbar({commit}, payload = {}) {
    commit(SET_SNACKBARS, payload);
  },
};
