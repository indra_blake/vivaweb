import {namespace} from 'vuex-class';

export const checkoutModule = namespace('checkout');

export const SET_PAYMENT_METHOD = 'SET_PAYMENT_METHOD';

export const SET_SHIPPING_METHOD = 'SET_SHIPPING_METHOD';

export const SET_CONFIRM_CHECKOUT = 'SET_CONFIRM_CHECKOUT';

export const SET_COUPON = 'SET_COUPON';

export const state = () => ({
  paymentMethod: [],
  shippingMethod: [],
  confirm: {},
  coupon: {},
});

export const mutations = {
  [SET_PAYMENT_METHOD](state, {data}) {
    state.paymentMethod = data;
  },
  [SET_SHIPPING_METHOD](state, {data}) {
    state.shippingMethod = data;
  },
  [SET_CONFIRM_CHECKOUT](state, {data}) {
    state.confirm = data;
  },
  [SET_COUPON](state, {data}) {
    state.coupon = data;
  },
};

export const actions = {
  async getPaymentMethod({commit}, params = {}) {
    try {
      const res = await this.$axios.get(
        `/api/payment-method/channels-list`,
        params,
      );

      commit(SET_PAYMENT_METHOD, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getShippingMethod({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/shipment-type/list`, {
        params,
      });

      commit(SET_SHIPPING_METHOD, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async confirmCheckout({commit}, params = {}) {
    try {
      const res = await this.$axios.post(
        `/api/order/${params.id}/confirm`,
        params.data,
      );
      commit(SET_CONFIRM_CHECKOUT, {
        data: res.data,
      });

      // commit(SET_STORE, {data: params.store_id});
      return res;
    } catch (e) {
      // console.log(e);
      commit(SET_CONFIRM_CHECKOUT, {
        data: e.response.data.message,
      });
      this.$snackbar.error(e.response.data.message);

      return e.response;
    }
  },
  async applyCoupon({commit}, params = {}) {
    try {
      const res = await this.$axios.$post(
        `/api/order/${params.id}/apply-voucher`,
        params.data,
      );
      commit(SET_COUPON, {
        data: res.data,
      });

      // commit(SET_STORE, {data: params.store_id});
      return res;
    } catch (e) {
      // console.log(e);
      commit(SET_COUPON, {
        data: {},
      });
      return e.response.data;
    }
  },
};
