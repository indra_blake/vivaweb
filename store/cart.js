import {namespace} from 'vuex-class';

export const cartModule = namespace('cart');

export const SET_CART = 'SET_CART';

export const ADD_PRODUCT_TO_CART = 'ADD_PRODUCT_TO_CART';

export const REMOVE_PRODUCT_FROM_CART = 'REMOVE_PRODUCT_FROM_CART';

export const REMOVE_ALL_PRODUCT_FROM_CART = 'REMOVE_ALL_PRODUCT_FROM_CART';

export const INCREASE_QTY_PRODUCT = 'INCREASE_QTY_PRODUCT';

export const DECREASE_QTY_PRODUCT = 'DECREASE_QTY_PRODUCT';

export const SET_COUNTER_CART = 'SET_COUNTER_CART';

export const SET_CHECKOUT_CART = 'SET_CHECKOUT_CART';

export const SET_STORE = 'SET_STORE';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  counter: 0,
  total: 0,
  cart: {},
  checkout: {},
  store: '',
});

export const mutations = {
  [SET_CART](state, {data}) {
    state.cart = data;
  },
  [ADD_PRODUCT_TO_CART](state, {data}) {
    state.cart = [...state.cart, data];
  },
  [REMOVE_PRODUCT_FROM_CART](state, {id}) {
    const productIndex = state.cart.findIndex((product) => product.id === id);
    const tempCart = [...state.cart];
    tempCart.splice(productIndex, 1);
    state.cart = tempCart;
  },
  [INCREASE_QTY_PRODUCT](state, {id}) {
    const cartItem = state.cart.find((item) => item.id === id);
    cartItem.quantity++;
  },
  [DECREASE_QTY_PRODUCT](state, {id}) {
    const cartItem = state.cart.find((item) => item.id === id);
    cartItem.quantity--;
  },
  [REMOVE_ALL_PRODUCT_FROM_CART](state) {
    state.cart = [];
  },
  [SET_COUNTER_CART](state) {
    const cart = {...state.cart};
    const totalCounter =
      typeof cart.cart_items !== 'undefined' && cart.cart_items.length > 0
        ? cart.cart_items.length
        : 0;

    state.counter = totalCounter;
  },
  [SET_CHECKOUT_CART](state, {data}) {
    state.checkout = data;
  },
  [SET_STORE](state, {data}) {
    state.store = data;
  },
};

export const actions = {
  async getCart({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/my/cart?${filter}`);
      commit(SET_CART, {
        data: res.data.data,
      });
      commit(SET_COUNTER_CART);
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async addProductToCart({commit}, params = {}) {
    try {
      const res = await this.$axios.post(`/api/cart/product/add`, params);
      commit(SET_CART, {
        data: res.data.data,
      });
      commit(SET_COUNTER_CART);
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async increaseQtyProduct({commit}, {id, params = {}}) {
    try {
      const res = await this.$axios.post(
        `/api/cart/product/increase?store_id=${id}`,
        params,
      );
      commit(SET_CART, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async decreaseQtyProduct({commit}, {id, params = {}}) {
    try {
      const res = await this.$axios.post(
        `/api/cart/product/decrease?store_id=${id}`,
        params,
      );
      commit(SET_CART, {
        data: res.data.data,
      });
      commit(SET_COUNTER_CART);
      return res;
    } catch (e) {
      return e.response;
    }
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async removeProductFromCart({commit}, params = {}) {
    try {
      const res = await this.$axios.post(`/api/cart/product/remove`, params);
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async removeAllProductFromCart({commit}) {
    try {
      await commit(REMOVE_ALL_PRODUCT_FROM_CART);
      await commit(SET_COUNTER_CART);
    } catch (e) {
      return e.response;
    }
  },

  async processCheckout({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/cart/product/checkout', params);

      commit(SET_CHECKOUT_CART, {
        data: res.data,
      });

      commit(SET_STORE, {data: params.store_id});
      return res;
    } catch (e) {
      commit(SET_CHECKOUT_CART, {
        data: e.response.data,
      });
      const res = e.response.data;
      const message = `${res?.message}: ${res?.data?.id}`;
      this.$snackbar.show(message, 'error', {
        link: '/order',
        linkLabel: 'DETAIL',
      });
      return e.response;
    }
  },
};
