import {namespace} from 'vuex-class';

export const articleModule = namespace('articles');

export const SET_ARTICLES = 'SET_ARTICLES';

export const SET_DETAIL_ARTICLE = 'SET_DETAIL_ARTICLE';

export const SET_TAGS = 'SET_TAGS';

export const SET_ARTICLE_META = 'SET_ARTICLE_META';

export const SET_ARTICLE_RELATED = 'SET_ARTICLE_RELATED';

const serialize = function (obj, prefix) {
  const str = [];
  for (const p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p;
      const v = obj[p];
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v),
      );
    }
  }
  return str.join('&');
};

export const state = () => ({
  articles: [],
  detail: {},
  tags: [],
  meta: {},
  releted: [],
});

export const mutations = {
  [SET_ARTICLES](state, {data, loadMore}) {
    if (loadMore) {
      state.articles = [...state.articles, ...data];
    } else {
      state.articles = data;
    }
  },
  [SET_TAGS](state, {data}) {
    state.tags = data;
  },
  [SET_DETAIL_ARTICLE](state, {data}) {
    state.detail = data;
  },
  [SET_ARTICLE_META](state, {data}) {
    state.meta = data;
  },
  [SET_ARTICLE_RELATED](state, {data}) {
    state.releted = data;
  },
};

export const actions = {
  async getArticles({commit}, params = {}) {
    try {
      const filter = serialize(params);

      const res = await this.$axios.get(`/api/articles?${filter}`);
      commit(SET_ARTICLES, {
        data: res.data.data.data,
        loadMore: params.loadMore,
      });
      commit(SET_ARTICLE_META, {
        data: {
          lastPage: res.data.data.lastPage,
          page: res.data.data.page,
          perPage: res.data.data.perPage,
          total: res.data.data.total,
        },
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetailArticle({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/articles/${id}`);
      commit(SET_DETAIL_ARTICLE, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getArticleReleted({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/articles/${id}/related-products`);
      commit(SET_ARTICLE_RELATED, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getTags({commit}) {
    try {
      const res = await this.$axios.get('/api/tags');
      commit(SET_TAGS, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
