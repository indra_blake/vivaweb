import {namespace} from 'vuex-class';

export const doctorsModule = namespace('doctors');

export const SET_DOCTORS = 'SET_DOCTORS';
export const SET_DOCTOR = 'SET_DOCTOR';
export const SET_REVIEWS = 'SET_REVIEWS';

// const serialize = function (obj, prefix?) {
//   const str = [];
//   for (const p in obj) {
//     // eslint-disable-next-line no-prototype-builtins
//     if (obj.hasOwnProperty(p)) {
//       const k: string = prefix ? prefix + '[' + p + ']' : p;
//       const v: any = obj[p];
//       str.push(
//         v !== null && typeof v === 'object'
//           ? serialize(v, k)
//           : encodeURIComponent(k) + '=' + encodeURIComponent(v),
//       );
//     }
//   }
//   return str.join('&');
// };

export interface Doctor {
  [key: string]: any;
}

export const state = () => ({
  doctors: [],
  doctor: {},
  reviews: [],
});

export const mutations = {
  [SET_DOCTORS](state, {data}) {
    state.doctors = data;
  },
  [SET_DOCTOR](state, data) {
    state.doctor = data;
  },
  [SET_REVIEWS](state, data) {
    state.reviews = data;
  },
};

export const actions = {
  async getDoctors({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/doctor`, {params});

      commit(SET_DOCTORS, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDoctorsConsultation({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/doctor/consultation`, {params});

      commit(SET_DOCTORS, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDoctorsAppointment({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/doctor/appointment`, {params});

      commit(SET_DOCTORS, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getDetail({commit}, id) {
    try {
      const {data} = await this.$axios.get(`/api/doctor/${id}`);
      commit(SET_DOCTOR, data.data);
      return data;
    } catch (e) {
      return e.response.data;
    }
  },
  async getReviews({commit}, id) {
    try {
      const {data} = await this.$axios.get(`/api/doctor/${id}/reviews`);
      commit(SET_REVIEWS, data.data);
      return data;
    } catch (e) {
      return e.response.data;
    }
  },
};
