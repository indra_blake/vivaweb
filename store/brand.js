import {namespace} from 'vuex-class';

export const brandModule = namespace('brand');

export const SET_POPULAR_BRAND = 'SET_POPULAR_BRAND';

export const SET_BRAND = 'SET_BRAND';

export const SET_META_BRAND = 'SET_META_BRAND';

export const SET_PRODUCT = 'SET_PRODUCT';
export const SET_DETAIL_BRAND = 'SET_DETAIL_BRAND';

export const state = () => ({
  brand: [],
  popular: [],
  meta: {},
  product: [],
  detail: {},
});

export const mutations = {
  [SET_BRAND](state, {data}) {
    state.brand = data;
  },
  [SET_POPULAR_BRAND](state, {data}) {
    state.popular = data;
  },
  [SET_META_BRAND](state, {data}) {
    state.meta = data;
  },
  [SET_PRODUCT](state, {data}) {
    state.product = data;
  },
  [SET_DETAIL_BRAND](state, {data}) {
    state.detail = data;
  },
};

export const actions = {
  async getBrand({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/brands`, {params});
      commit(SET_BRAND, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getPopularBrand({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/brands/popular`, params);
      commit(SET_POPULAR_BRAND, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getBrandProduct({commit}, params = {}) {
    try {
      const res = await this.$axios.$get(
        `/api/brands/${params.id}/products`,
        params.data,
      );
      commit(SET_PRODUCT, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },

  async getBrandDetail({commit}, id) {
    try {
      const res = await this.$axios.$get(`/api/brands/${id}`);
      commit(SET_DETAIL_BRAND, {
        data: res.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
};
