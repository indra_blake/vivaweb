import {namespace} from 'vuex-class';

export const userModule = namespace('users');

export const SET_TRACKER = 'SET_TRACKER';
export const SET_TRACKERS = 'SET_TRACKERS';

export const SET_BMI = 'SET_BMI';

export const state = () => ({
  tracker: {},
  trackers: [],
  bmi: 0,
});

export const mutations = {
  [SET_TRACKER](state, {data}) {
    state.tracker = data;
  },
  [SET_TRACKERS](state, {data}) {
    state.trackers = data;
  },
  [SET_BMI](state, {data}) {
    state.bmi = data;
  },
};

export const actions = {
  async register(_, payload = {}) {
    try {
      return await this.$axios.post('/api/register', payload);
    } catch (e) {
      return e.response;
    }
  },
  async forgotPassword(_, payload = {}) {
    try {
      return await this.$axios.post('/api/reset-password', payload);
    } catch (e) {
      return e.response;
    }
  },
  async confirmOTP(_, payload = {}) {
    try {
      return await this.$axios.post('/api/confirm-otp', payload);
    } catch (e) {
      return e.response;
    }
  },
  async resetPassword(_, payload = {}) {
    try {
      return await this.$axios.post('/api/set-password', payload);
    } catch (e) {
      return e.response;
    }
  },
  async logout(_, payload = {}) {
    try {
      return await this.$axios.post('/api/logout', payload);
    } catch (e) {
      return e.response;
    }
  },
  async getTracker({commit}) {
    try {
      const res = await this.$axios.get('api/my/health-tracker');
      commit(SET_TRACKER, {
        data: res.data.data,
      });
    } catch (e) {
      return e.response;
    }
  },
  async getTrackerByName({commit}, params) {
    try {
      if (params.name === 'bmi') {
        const res = await this.$axios.get('api/bmi', {
          params,
        });
        commit(SET_TRACKERS, {
          data: res.data.data.data.sort((a, b) => {
            const aDate = a.created_at
              ? this.$dayjs(`${a.created_at}`)
              : this.$dayjs('1970-01-01');
            const bDate = b.created_at
              ? this.$dayjs(`${b.created_at}`)
              : this.$dayjs('1970-01-01');

            return aDate - bDate;
          }),
        });
      } else {
        const res = await this.$axios.get('api/my/health-tracker/by-name', {
          params,
        });
        commit(SET_TRACKERS, {
          data: res.data.data.data.sort((a, b) => {
            const aDate = a.date
              ? this.$dayjs(`${a.date}`)
              : this.$dayjs('1970-01-01');
            const bDate = b.date
              ? this.$dayjs(`${b.date}`)
              : this.$dayjs('1970-01-01');

            return aDate - bDate;
          }),
        });
      }
    } catch (e) {
      return e.response;
    }
  },
  async calculateBmi({commit}, params) {
    try {
      const res = await this.$axios.$post('api/bmi/calculator', params);
      commit(SET_BMI, {
        data: res.data.calculated,
      });
    } catch (e) {
      return e.response;
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async saveBmi({commit}, params) {
    try {
      const res = await this.$axios.$post('api/bmi', params);
      return res;
    } catch (e) {
      return e.response;
    }
  },
};
