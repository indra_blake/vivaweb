import {namespace} from 'vuex-class';

export const messageModule = namespace('message');

export const SET_NONCE = 'SET_NONCE';

export const SET_NEW_ROOM = 'SET_NEW_ROOM';

export const SET_TOKEN = 'SET_TOKEN';

export const SET_ROOMS = 'SET_ROOMS';

export const SET_CHAT_ROOMS = 'SET_CHAT_ROOMS';

export const SET_USER = 'SET_USER';

export const SET_NEW_MESSAGE = 'SET_NEW_MESSAGE';

export const SET_SELECTED_ROOM = 'SET_SELECTED_ROOM';

export const SET_TYPING = 'SET_TYPING';

export const SET_DELIVERED_MESSAGE = 'SET_DELIVERED_MESSAGE';

export const SET_READ_MESSAGE = 'SET_READ_MESSAGE';
export const SET_SENDMESSAGE = 'SET_SENDMESSAGE';

export const state = () => ({
  nonce: {},
  token: '',
  rooms: [],
  selectedRoom: {},
  user: {},
  newMessage: {},
  isTyping: 0,
  newRoom: {},
  readMessage: {},
  sendMessage: [],
  deliveredMessage: {},
  chatRooms: [],
});

export const mutations = {
  [SET_NONCE](state, {data}) {
    state.nonce = data;
  },
  [SET_TOKEN](state, {data}) {
    state.token = data;
  },
  [SET_ROOMS](state, {data}) {
    state.rooms = data;
  },
  [SET_USER](state, {data}) {
    state.user = data;
  },
  [SET_NEW_MESSAGE](state, {data}) {
    state.newMessage = data;
  },
  [SET_SELECTED_ROOM](state, {data}) {
    state.selectedRoom = data;
  },
  [SET_NEW_ROOM](state, {data}) {
    state.newRoom = data;
  },
  [SET_TYPING](state, {data}) {
    state.isTyping = data;
  },
  [SET_READ_MESSAGE](state, {data}) {
    state.readMessage = data;
  },
  [SET_DELIVERED_MESSAGE](state, {data}) {
    state.deliveredMessage = data;
  },
  [SET_CHAT_ROOMS](state, {data}) {
    state.chatRooms = data;
  },
  [SET_SENDMESSAGE](state, {data}) {
    state.sendMessage = data;
  },
};

export const actions = {
  async getNonce({commit}) {
    let dNonce;
    try {
      const res = await this.$qiscus.getNonce().then(
        (res) => {
          dNonce = res;
        },
        (err) => {
          return err;
        },
      );
      commit(SET_NONCE, {
        data: dNonce,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getToken({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/chat/create-token', params);
      commit(SET_TOKEN, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getRooms({commit}, params = {}) {
    try {
      const res = await this.$axios.get('/api/chat', params);
      commit(SET_ROOMS, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      commit(SET_ROOMS, {
        data: [],
      });
      return e.response;
    }
  },
  async getUser({commit}, params) {
    try {
      const res = await this.$qiscus.verifyIdentityToken(params);
      commit(SET_USER, {
        data: res,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  getNewMessage({commit}, message) {
    commit(SET_NEW_MESSAGE, {
      data: message,
    });
    return message;
  },
  getSelectedRoom({commit}, room) {
    commit(SET_SELECTED_ROOM, {
      data: room,
    });
    return room;
  },
  async getNewRoom({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/chat', params);
      const newRoom = res.data.data;
      if (newRoom) {
        commit(SET_NEW_ROOM, {
          data: newRoom,
        });
      } else {
        commit(SET_NEW_ROOM, {
          data: res.data.data.rooms[0],
        });
      }
      return res;
    } catch (e) {
      return e.response;
    }
  },

  getTyping({commit}, typing) {
    try {
      this.$qiscus.publishTyping(typing);
      commit(SET_TYPING, {
        data: typing,
      });
      return typing;
    } catch (e) {
      return e.response;
    }
  },
  getDeliveredMessage({commit}, message) {
    commit(SET_DELIVERED_MESSAGE, {
      data: message,
    });
    return message;
  },
  getReadMessage({commit}, message) {
    commit(SET_READ_MESSAGE, {
      data: message,
    });
    return message;
  },
  async getChatRooms({commit}, ids) {
    try {
      const res = await this.$qiscus.getRoomsInfo({
        room_ids: ids,
      });
      commit(SET_CHAT_ROOMS, {
        data: res.results.rooms_info.sort(function (x, y) {
          return y.last_comment.unix_timestamp - x.last_comment.unix_timestamp;
        }),
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async readMessagePharmacy({commit}, {id, params = {}}) {
    try {
      const res = await this.$axios.$post(
        `/api/chat/${id}/set-replied`,
        params,
      );
      commit(SET_SENDMESSAGE, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
  async readMessage({commit}, id) {
    try {
      const res = await this.$axios.$get(`/api/chat/${id}/set-read`);
      commit(SET_SENDMESSAGE, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
};
