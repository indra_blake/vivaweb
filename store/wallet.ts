import {namespace} from 'vuex-class';

export const walletModule = namespace('wallet');

export const SET_WALLETS = 'SET_WALLETS';
export const SET_WALLET = 'SET_WALLET';
export const SET_EXPIRE = 'SET_EXPIRE';
export const SET_ACCOUNT = 'SET_ACCOUNT';
export const SET_WALLETSTATUS = 'SET_WALLETSTATUS';
export const SET_HISTORYDATE = 'SET_HISTORYDATE';
export const SET_HISTORYDETAIL = 'SET_HISTORYDETAIL';
export const SET_ORDER_OTP = 'SET_ORDER_OTP';
export const SET_GENERATEOTPORDER = 'SET_GENERATEOTPORDER';
export const SET_PAYORDER = 'SET_PAYORDER';

export interface Wallet {
  [key: string]: any;
}

export const state = () => ({
  wallets: [],
  wallet: {},
  expired: [],
  account: [],
  userWallet: [],
  historyWallet: [],
  detailItems: [],
  orderOtp: [],
  generateOTP: [],
  payOrder: [],
});

export const mutations = {
  [SET_WALLETS](state, {data}) {
    state.wallets = data;
  },
  [SET_WALLET](state, data) {
    state.wallet = data;
  },
  [SET_EXPIRE](state, data) {
    state.expired = data;
  },
  [SET_ACCOUNT](state, data) {
    state.account = data;
  },
  [SET_WALLETSTATUS](state, data) {
    state.userWallet = data;
  },
  [SET_HISTORYDATE](state, data) {
    state.historyWallet = data.data;
  },
  [SET_HISTORYDETAIL](state, data) {
    state.detailItems = data.data;
  },
  [SET_ORDER_OTP](state, data) {
    state.orderOtp = data.data;
  },
  [SET_GENERATEOTPORDER](state, data) {
    state.generateOTP = data.data;
  },
  [SET_PAYORDER](state, data) {
    state.payOrder = data.data;
  },
};

export const actions = {
  async getListVoucher({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/voucher`, {params});

      commit(SET_WALLETS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getWalletProfile({commit}) {
    try {
      const res = await this.$axios.get(`/api/wallet/get-balance`);

      commit(SET_WALLETSTATUS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getWalletTransaction({commit}, params) {
    try {
      const res = await this.$axios.post(
        `/api/wallet/report-transaction`,
        params,
      );

      commit(SET_HISTORYDATE, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getMyAccount({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/my/profile`, {params});

      commit(SET_ACCOUNT, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postPreRegist({commit}, params) {
    try {
      const res = await this.$axios.post(`/api/wallet/pre-registration`, {
        mobile_number: params,
      });

      commit(SET_EXPIRE, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postRegistration({commit}, params) {
    try {
      const res = await this.$axios.post(
        `/api/wallet/confirm-registration`,
        params,
      );

      commit(SET_WALLETS, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async validOTP({commit}, params = {}) {
    try {
      const res = await this.$axios.post(`/api/wallet/validate-otp`, {
        params,
      });

      commit(SET_WALLET, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postGetOrderOTP({commit}) {
    try {
      const res = await this.$axios.post(`/api/wallet/get-otp`);

      commit(SET_ORDER_OTP, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postGenerateOTP({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/wallet/payment-token`, {params});

      commit(SET_GENERATEOTPORDER, {
        data: res.data.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
  async postPayOrdeWallet({commit}, params) {
    try {
      const res = await this.$axios.post(`/api/wallet/pay-order`, params);

      commit(SET_PAYORDER, {
        data: res.data.data,
      });

      return res;
    } catch (e) {
      return e.response;
    }
  },
};
