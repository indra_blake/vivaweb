import {namespace} from 'vuex-class';

export const consultationModule = namespace('consultation');

export const SET_CONSULTATION = 'SET_CONSULTATION';

export const SET_CONSULTATIONS = 'SET_CONSULTATIONS';

export const SET_REVIEWS = 'SET_REVIEWS';
export const SET_REVIEW = 'SET_REVIEW';
export const SET_READMESSAGE = 'SET_READMESSAGE';
export const SET_SENDMESSAGE = 'SET_SENDMESSAGE';
export const SET_ENDMESSAGE = 'SET_ENDMESSAGE';

export const state = () => ({
  consultation: {},
  consultations: [],
  reviews: [],
  review: {},
  readMessage: [],
  sendMessage: [],
  endMessage: [],
});

export const mutations = {
  [SET_CONSULTATION](state, {data}) {
    state.consultation = data;
  },
  [SET_CONSULTATIONS](state, {data}) {
    state.consultations = data;
  },
  [SET_REVIEWS](state, {data}) {
    state.reviews = data;
  },
  [SET_REVIEW](state, {data}) {
    state.review = data;
  },
  [SET_READMESSAGE](state, {data}) {
    state.readMessage = data;
  },
  [SET_SENDMESSAGE](state, {data}) {
    state.sendMessage = data;
  },
  [SET_ENDMESSAGE](state, {data}) {
    state.endMessage = data;
  },
};

export const actions = {
  async createConsultation({commit}, params = {}) {
    try {
      const res = await this.$axios.post('/api/consultation', params);
      commit(SET_CONSULTATION, {
        data: res.data,
      });
      return res.data;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
  async purchaseConsultation({commit}, params = {}) {
    try {
      const res = await this.$axios.$post('/api/consultation/purchase', params);
      commit(SET_CONSULTATION, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
  async getConsultations({commit}, params = {}) {
    try {
      const res = await this.$axios.get(`/api/consultation`, {params});
      commit(SET_CONSULTATIONS, {
        data: res.data.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getDetail({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/consultation/${id}`);
      commit(SET_CONSULTATION, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async getReview({commit}, id) {
    try {
      const res = await this.$axios.get(`/api/consultation/review/${id}`);
      commit(SET_REVIEW, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async createReview({commit}, {id, data}) {
    try {
      const res = await this.$axios.post(
        `/api/consultation/${id}/review`,
        data,
      );
      commit(SET_REVIEW, {
        data: res.data.data,
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  async readMessageDoctor({commit}, {id, params = {}}) {
    try {
      const res = await this.$axios.$get(
        `/api/consultation/${id}/read`,
        params,
      );
      commit(SET_READMESSAGE, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
  async sendMessageDoctor({commit}, id) {
    try {
      const res = await this.$axios.$post(`/api/consultation/${id}/replied`);
      commit(SET_SENDMESSAGE, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
  async endChatConsultation({commit}, id: number | string) {
    try {
      const res = await this.$axios.$post(`/api/consultation/${id}/end`);
      commit(SET_ENDMESSAGE, {
        data: res.data,
      });
      return res;
    } catch (e) {
      this.$snackbar.error(e);
      return e.response;
    }
  },
};
