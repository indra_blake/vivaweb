import {Snackbar} from '~/utils/Snackbar';

declare module 'vue/types/vue' {
  interface Vue {
    $snackbar: Snackbar;
  }

  interface VueConstructor {
    $snackbar: Snackbar;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    $snackbar: Snackbar;
  }
}
