export interface City {
  City_ActiveYN: string;
  City_AreaNumber: number | string | null;
  City_ID: number;
  City_LastUpdate: string;
  City_Name: string;
  City_ProvID: number;
  City_UserID: string;
}
