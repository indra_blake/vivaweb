export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ssr: true,
  // mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: 'VIVA Apotek',
    titleTemplate: '%s - VIVA Apotek',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {
        hid: 'description',
        name: 'description',
        content: 'One application to meet your health needs',
      },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        content: 'VIVA Apotek Online',
      },
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap',
      },
    ],
  },
  /*
   ** Global CSS
   */
  css: [
    '~/assets/app.scss',
    // '~/node_modules/bootstrap/dist/css/bootstrap.css',
    // '~/node_modules/bootstrap-vue/dist/bootstrap-vue.css'
  ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~/plugins/vee-validate.ts',
    '~/plugins/slick-carousel.ts',
    '~/plugins/dayjs.ts',
    '~/plugins/moment.ts',
    '~/plugins/geolib.ts',
    '~/plugins/snackbar.ts',
    '~/plugins/chat-scroll.ts',
    '~/plugins/vue-gtag.ts',
    '~/plugins/axios.ts',
    {
      src: '~/plugins/numeral.ts',
      mode: 'client',
    },
    {
      src: '~/plugins/qiscus.ts',
      mode: 'client',
    },
    '~/plugins/v-clipboard.ts',
    {
      src: '~/plugins/chart.ts',
      mode: 'client',
    },
  ],
  // https://nuxtjs.org/docs/configuration-glossary/configuration-servermiddleware/
  serverMiddleware: [
    {
      path: '/.well-known/apple-app-site-association',
      handler: '@/middleware/apple.js',
    },
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
    '@nuxtjs/fontawesome',
    '@nuxtjs/svg',
    '@nuxtjs/moment',
    '@nuxtjs/dayjs',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxt/content',
    [
      'nuxt-gmaps',
      {
        key: 'AIzaSyBUA6gvxq8GkiCmyFFgNqAMQipTQolBAnE',
        libraries: ['places'],
      },
    ],
    'vue-social-sharing/nuxt',
    '@nuxtjs/cloudinary',
    '@nuxtjs/firebase',
    'nuxt-i18n',
    'nuxt-clipboard',
  ],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  // googleAnalytics: {
  //   id: 'G-VBW2X0P0WJ',
  // },
  clipboard: {
    autoSetContainer: true,
  },
  vuetify: {
    customVariables: ['~/assets/vuetify.scss'],
    theme: {
      themes: {
        light: {
          primary: '#E21683',
          secondary: '#EE7421',
          black1: '#333333',
          grey3: '#828282',
          grey4: '#B8BCC6',
        },
      },
    },
    treeShake: true,
    defaultAssets: {
      font: {
        family: 'Montserrat',
      },
    },
    options: {
      customProperties: true,
    },
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // proxy: true,
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    transpile: ['vee-validate/dist/rules', 'vue-slick-carousel'],
    babel: {
      plugins: [
        // 'transform-decorators-legacy',
        'transform-class-properties',
      ],
    },
    // vendor: ['vue-slick-carousel'],
  },
  /**
   * Nuxt auth config
   */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/login',
            method: 'post',
            propertyName: 'data.authentication.token',
          },
          logout: false,
          user: {
            url: '/api/my/profile',
            method: 'get',
            propertyName: 'data',
          },
        },
        autoFetchUser: false,
        // tokenRequired: false,
        // tokenType: false,
      },
    },
    redirect: {
      login: '/auth',
      logout: '/',
      callback: '/auth',
      home: '/',
    },
  },
  fontawesome: {
    icons: {
      brands: [
        'faFacebook',
        'faInstagram',
        'faTwitter',
        'faYoutube',
        'faLinkedin',
        'faWhatsapp',
      ],
      solid: [
        'faUser',
        'faPhone',
        'faChevronRight',
        'faStar',
        'faTrash',
        'faIdCard',
      ],
    },
  },
  loading: {
    color: '#EE7421',
    // height: '5px',
  },
  /**
   * Cloudinary Options
   */
  cloudinary: {
    useComponent: true,
    cloudName: process.env.CLOUDINARY_CLOUD_NAME || 'vivapotekd',
    apiKey: process.env.CLOUDINARY_API_KEY || '695648723926266',
    apiSecret:
      process.env.CLOUDINARY_API_SECRET || 'cYIhNKJ5zYV9kCifnYT80I2ZzDE',
  },
  env: {
    MAPS_API_KEY:
      process.env.MAPS_API_KEY || 'AIzaSyDiaq3HT2M8vXwP0YesnuaMjRMKuc6HWHY',
  },
  // https://firebase.nuxtjs.org/guide/getting-started
  firebase: {
    config: {
      apiKey: process.env.FIREBASE_API_KEY,
      authDomain: process.env.FIREBASE_AUTH_DOMAIN,
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      projectId: process.env.FIREBASE_PROJECT_ID,
      storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
      appId: process.env.FIREBASE_APP_ID,
      measurementId: process.env.FIREBASE_MEASUREMENT_ID,
      fcmPublicVapidKey: process.env.FIREBASE_FCM_PUBLIC_VAPID_KEY, // OPTIONAL : Sets vapid key for FCM after initialization
    },
    services: {
      messaging: {
        createServiceWorker: true,
        actions: [
          {
            action: 'goToUrl',
            url: 'https://gits.id',
          },
        ],
      },
    },
  },
  publicRuntimeConfig: {
    apiURL: process.env.API_URL,
  },
  i18n: {
    defaultLocale: 'id',
    locales: [
      {
        code: 'en',
        file: 'en.json',
        name: 'English',
      },
      {
        code: 'id',
        file: 'id.json',
        name: 'Indonesia',
      },
    ],
    lazy: true,
    langDir: 'lang/',
    strategy: 'no_prefix',
  },
  dayjs: {
    locales: ['en', 'id'],
    defaultLocale: 'en',
    defaultTimeZone: 'Asia/Jakarta',
    plugins: [
      'relativeTime',
      'timezone', // import 'dayjs/plugin/utc',
      'isBetween',
    ], // Your Day.js plugin
  },
  moment: {
    // option
  },
};
