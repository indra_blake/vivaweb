import {Component, Vue} from 'vue-property-decorator';
import ResetPasswordStorage from '~/utils/ResetPasswordStorage';

@Component
export default class PasswordResetMixin extends Vue {
  mounted() {
    if (!new ResetPasswordStorage().getPhoneNumber()) {
      this.$router.push('/auth/password');
    }
  }
}
