import {Component, Vue} from 'vue-property-decorator';
import {messageModule} from '@/store/message';

@Component
export default class MenusMixin extends Vue {
  [x: string]: any;
  @messageModule.State('rooms') rooms;

  get items() {
    return [
      {
        title: this.$t('nav_home'),
        to: '/',
      },
      {
        title: this.$t('nav_article'),
        to: '/articles',
      },
      {
        title: this.$t('nav_message'),
        to: '/messages/pharmacy',
      },
      {
        title: this.$t('nav_order'),
        to: '/order',
      },
      {
        title: this.$t('consultation'),
        to: '/doctor/consultation',
      },
      {
        title: this.$t('appointment'),
        to: '/doctor/appointment',
      },
      {
        title: this.$t('pharmacy'),
        to: '/pharmacy',
      },
    ];
  }
}
