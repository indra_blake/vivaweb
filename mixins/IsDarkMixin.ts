import {Component, Vue, Watch} from 'vue-property-decorator';

@Component
export default class IsDarkMixin extends Vue {
  get isDark() {
    return this.$store.state.isDark;
  }

  set isDark(val: boolean) {
    this.$store.commit('setDark', val);
  }

  @Watch('$route')
  onRouteChanged(route: any) {
    this.isDark = route.name === 'index';
  }
}
