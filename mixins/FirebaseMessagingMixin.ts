import {Component, Vue} from 'vue-property-decorator';

@Component
export default class FirebaseMessagingMixin extends Vue {
  listenersStarted = false;
  permissionGranted = false;
  idToken = '';
  loading = false;

  async requestPermission() {
    try {
      const permission = await Notification.requestPermission();
      this.permissionGranted = permission === 'granted';
      this.onPermissionGranted();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }

  onPermissionGranted() {
    // eslint-disable-next-line no-console
    console.log('permission granted');
    this.getIdToken();
  }

  async getIdToken() {
    this.loading = true;
    let currentToken;
    try {
      currentToken = await this.$fire.messaging.getToken();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error('An error occurred while retrieving token. ', e);
      this.idToken = '';
      this.loading = false;
    }
    if (currentToken) {
      this.idToken = currentToken;
    } else {
      // Show permission request.
      // eslint-disable-next-line no-console
      console.info(
        'No Instance ID token available. Request permission to generate one.',
      );
      // Show permission UI.
      // updateUIForPushPermissionRequired();
      this.idToken = '';
    }
    // eslint-disable-next-line no-console
    console.log(`The id token is: ${this.idToken}`);
    this.loading = false;
  }

  startListeners() {
    this.startOnMessageListener();
    this.startTokenRefreshListener();
    this.listenersStarted = true;
  }

  startOnMessageListener() {
    this.$fire.messaging.onMessage((payload) => {
      // eslint-disable-next-line no-console
      console.info('Message received. ', payload);
    });
  }

  startTokenRefreshListener() {
    this.$fire.messaging.onTokenRefresh(async () => {
      try {
        this.idToken = await this.$fire.messaging.getToken();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error('Unable to retrieve refreshed token ', e);
      }
    });
  }

  sendTestMessage() {
    try {
      setTimeout(() => {
        // wait 5 seconds so you have time to switch away from the page to test the service-worker
        this.$fire.functions.httpsCallable('sendTestPushMessage')({
          token: this.idToken,
        });
      }, 5000);
    } catch (e) {
      alert(e);
    }
  }
}
