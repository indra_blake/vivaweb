import {Component, Vue} from 'vue-property-decorator';
import WalletStorage from '~/utils/walletStorage.ts';

@Component
export default class WalletMixin extends Vue {
  mounted() {
    if (!new WalletStorage().getPhoneNumber()) {
      this.$router.push('/wallet');
    }
  }
}
