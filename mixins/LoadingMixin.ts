import {Component, Vue} from 'vue-property-decorator';

@Component
export default class LoadingMixin extends Vue {
  get isLoading() {
    return this.$nuxt.$loading?.$data?.percent > 0;
  }
}
