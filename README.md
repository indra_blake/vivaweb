# VivaHealth Web

## Tech Stack Used

Core:

- [Vue.js](https://vuejs.org/)
- [Nuxt.js](https://nuxtjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Vue Router](https://router.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Vue Class Component](https://github.com/vuejs/vue-class-component)
- [Vue Property Decorator](https://github.com/kaorun343/vue-property-decorator)

Nuxt Plugins:

- [Nuxt Axios](https://axios.nuxtjs.org/)
- [Nuxt Store (Vuex)](https://id.nuxtjs.org/guide/vuex-store/)
- [Nuxt Auth](https://auth.nuxtjs.org/)
- [Nuxt Class Component](https://github.com/nuxt-community/nuxt-class-component)
- [Nuxt PWA](https://pwa.nuxtjs.org/)
- [Nuxt Vuetify](https://github.com/nuxt-community/vuetify-module)

Other Plugins:
- Form validation with [vee-validate](https://logaretm.github.io/vee-validate/)
- Date manipulation with [Day.js](https://day.js.org/en/)
- Awesome phonenumber parser with [awesome-phonenumber](https://github.com/grantila/awesome-phonenumber)
- Cookie handler with [js-cookie](https://github.com/js-cookie/js-cookie)

UI Framework: [Vuetify](https://vuetifyjs.com/en/)

Icons:
 - [Material Design Icons](https://materialdesignicons.com/)
 - Feather Icons via [vue-feather-icons](https://github.com/egoist/vue-feather-icons)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
