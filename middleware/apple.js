const appleJson = require('../static/.well-known/apple-app-site-association.json');
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handler = (req, res, next) => {
  // eslint-disable-line @typescript-eslint/no-unused-vars
  res.setHeader('Content-type', 'application / json; charset=utf-8');
  res.statusCode = 200;
  res.end(JSON.stringify(appleJson));
};

export default handler;
